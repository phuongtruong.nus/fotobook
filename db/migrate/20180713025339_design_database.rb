class DesignDatabase < ActiveRecord::Migration[5.2]
  def change
  	create_table :photos do |t|
      t.string :title
      t.text :description
      t.boolean :private, default: false
      t.belongs_to :user
      t.timestamps
    end

    create_table :albums do |t|
      t.string :title
      t.text :description
      t.boolean :private, default: false
      t.belongs_to :user
      t.timestamps
    end

    create_table :likes do |t|
      t.belongs_to :user
      t.belongs_to :likeable, polymorphic: true
      t.timestamps
    end

    create_table :relations do |t|
      t.belongs_to :follower
      t.belongs_to :following
      t.timestamps
    end

    create_table :images do |t|
      t.string :image_url, default: ''
      t.belongs_to :imageable, polymorphic: true
      t.timestamps
    end
  end
end
