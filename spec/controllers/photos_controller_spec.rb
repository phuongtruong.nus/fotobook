require 'rails_helper'

RSpec.describe PhotosController, type: :controller do
	describe "GET index" do
	    it "assigns @photos" do
	    	user = create(:user)
	     	photo = FactoryGirl.create(:photo)
	      	get :index
	     	expect(assigns(:photos)).to eq([photo])
	    end
	    it "renders the index template" do
	      get :index
	      expect(response).to render_template("index")
	    end
	end

	describe "GET #show" do
    	it "renders the #show view" do
     	 	get :show, {id: @photo.id}
     	 	response.should render_template :show
   		 end
  	end

  	describe "GET #new" do
    	it "renders the #new view" do
     	 	get :new
     	 	response.should render_template :new
   		 end
  	end
end
