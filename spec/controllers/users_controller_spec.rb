require 'rails_helper'

RSpec.describe UsersController, type: :controller do
	describe "GET #show" do
    	it "renders the #show view" do
     	 	get :show, {id: @user.id}
     	 	response.should render_template :show
   		 end
  	end
end
