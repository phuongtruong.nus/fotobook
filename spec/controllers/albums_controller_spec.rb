require 'rails_helper'

RSpec.describe AlbumsController, type: :controller do
	describe "GET index" do
	    it "assigns @albums" do
	      album = FactoryGirl.create(:album)
	      get :index
	      expect(assigns(:albums)).to eq([user])
	    end
	    it "renders the index template" do
	      get :index
	      expect(response).to render_template("index")
	    end
	end
	describe "GET #show" do
    	it "renders the #show view" do
     	 	get :index, {id: @album.id}
     	 	response.should render_template :show
   		 end
  	end

  	describe "GET #new" do
    	it "renders the #new view" do
     	 	get :new
     	 	response.should render_template :new
   		 end
  	end

  	describe "GET discover" do
    	it "renders the discover photo view" do
     	 	get :discover_album
     	 	response.should render_template :discover_album
   		end
  	end

    describe "GET stories#show" do
      it "should render stories#show template" do
        user = create(:user)
        album = create(:album, user: user)
        login_as(user, scope: :user)
        visit discover_albums_path
        page.should have_content(story.title)
        page.should have_content(story.desciption)
      end
    end
end
