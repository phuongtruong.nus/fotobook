Rails.application.routes.draw do
	devise_for :users, :controllers => { :omniauth_callbacks => "callbacks" } , path: "auth"

	resources :albums, except: [:index]
	resources :photos , except: [:index]
	resources :users, only: [:show]
 	authenticated :users do
 		root 'users#show'
	end
	unauthenticated :users do
	    root 'photos#discover_photos'
	end

	namespace :admin do
      	resources :albums, except: [:new, :create]
      	resources :users, except: [:new, :create]
      	resources :photos, except: [:new, :create]
      	delete '/delete/images' => 'albums#delete_images', as: :delete_images
		delete '/album/:album_id/delete/:image_id/images_old/' => 'albums#delete_image_attachment', as: :delete_images_old
    end

	get '/feed/photos' => 'photos#feed_photos'
	get '/feed/albums' => 'albums#feed_albums'

	get '/discover/photos' => 'photos#discover_photos'
	get '/discover/albums' => 'albums#discover_albums'

	delete '/delete/images' => 'albums#delete_images', as: :delete_images
	delete '/album/:album_id/delete/:image_id/images_old/' => 'albums#delete_image_attachment', as: :delete_images_old
	
	post '/user/:id/follow_user', to: 'relations#follow_user', as: :follow_user
  	post '/user/:id/unfollow_user', to: 'relations#unfollow_user', as: :unfollow_user

  	post '/:likeable_id/:likeable_type/like', to: 'likes#like', as: :like
  	post '/:likeable_id/:likeable_type/unlike', to: 'likes#unlike', as: :unlike
end
