# NUS REVIEW CODE RESULT FOR FINAL APP

### Issues
---------

#### Architecture



#### Security


#### Performance


#### Coding conventions & best practices

1/ Should specify version when adding a gem into Gemfile -- done

  - gem 'devise'
  - gem 'omniauth-twitter'
  - etc.


2/ As a trainee, should not use gem 'acts_as_follower' to implement follow model. You will gain much knowledge if you design and implement this module by yourself -- done


3/ Elements should be consistent if they are same type. -- done

  - http://localhost:3000/users/sign_in

    + Button "Login" smaller than other buttons. -- done
    + "Login" vs "Sign in" vs "Log in" -> should pick only one. -- done
    + The color of the links ("Create an account", etc.) should not be same as the color of the lable ("Email", etc.) -- done

4/ Must have notify message if user fail/successfully to make an action -- done

  - Login unsuccessfully does not have any error message -> user does not understand why he couldn't login.
  - Signup successfully and redirect to user to login page but there is no message to let him know he need to confirm his email address.
  - Forgot password.

5/ Need to have validation on both of frontend and backend

  - Login
  - Signup
  - Forgot pass
  - Etc.


6/ Why we use both of gem `font-awesome-rails` and `<link href="//maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">` and `<link rel="stylesheet" href="path/to/font-awesome/css/font-awesome.min.css">` (this method causes an error while loading this file) ?. Shoud pick only one way to load `font-awesome`.-- done


7/ Do not commit the file which are not in used -- done

  - app/assets/javascripts/home.js.coffee
  - app/assets/stylesheets/session.scss
  - etc.


8/ Inconsistent in coding style: -- done

  - Having different indentation between files:

    + app/assets/stylesheets/_form_login_social.scss (4 spaces)
    + app/assets/stylesheets/application.css.scss (2 spaces)


9/ Avoid using `!important` in CSS


10/ Avoid using `pt` in CSS for screen


11/ In CSS, do not need to put the unit following the zero. -- done

  - app/assets/stylesheets/users.scss

  => `border: 0px;` should be `border: 0;`


12/ In CSS, should not use unit percentage for margin/padding


13/ Do not use instance variable and avoid using `current_user` in partial

  - app/views/share/_upload_albums.html.erb
  - app/views/share/_upload_photos.html.erb
  - etc.


14/ Why does action `show` represent list of objects? I should represent only on object -- done

  - app/controllers/albums_controller.rb
  - app/controllers/photos_controller.rb


15/ Coding stype is too ugly: mess up indentation. --done

  - app/controllers/relations_controller.rb
  - app/controllers/users_controller.rb


16/ Prefer using the power of association instead of ActiveRecord Query -- done

  - app/views/albums/index.html.erb

  ```
  <%= User.find(alb.user_id).first_name + " " + User.find(alb.user_id).last_name %>
  ```
  should be
  ```
  <%= alb.user.first_name + " " + alb.user.last_name %>
  ```

  - app/views/share/_upload_albums.html.erb:10
  - app/views/share/_upload_photos.html.erb:14

17/ The view should not contain any logics. --done 

  - app/views/albums/index.html.erb

  ```
  <%= User.find(alb.user_id).first_name + " " + User.find(alb.user_id).last_name %>
  ```
  should be
  ```
  # user.rb
  def full_name
    "#{first_name} #{last_name}"
  end

  # index.html.erb
  <%= alb.user.full_name %>
  ```

  - app/views/layouts/application.html.erb:23
  - app/views/devise/mailer/reset_password_instructions.html.erb:1
  - app/views/photos/index.html.erb:13
  - app/views/share/_show_follower.html.erb:10
  - app/views/share/_show_following.html.erb:10
  - app/views/share/_upload_albums.html.erb:10
  - app/views/share/_upload_photos.html.erb:14
  - app/views/share/_user_profile.html.erb:11


18/ Duplicate code --done

  - app/views/share/_show_following.html.erb + app/views/share/_show_follower.html.erb

  => 2 these partials are almost exactly same. Why don't use only one partial and pass the variable to that partial instead of using `current_user.`

19/ Routing is too bad -- done

  - Prefer using resourceful than custom route
  - `albums` and `photos` must be `resources`, not `resource`


20/ Do not use external images/css in our CSS. Download them and use as internal images/css -- done

  - app/assets/stylesheets/_form_login_social.scss
  - app/views/layouts/application.html.erb


21/ Bootrap css and js are loaded many times in the app because of the declaration in application.js/application.scss and the layout. TIP: should use gem to help us integrate boostrap 4 into the app. A popular gem for bootstrap 4 is: https://github.com/twbs/bootstrap-rubygem


22/ Strange code -- done

  - app/controllers/albums_controller.rb#index

  => Why do we need to declare `@full_name` in this action although we can access `current_user` in any views/helpers? The second problem: we should not have `full_name` follow that way, please take a look at the issue #17 of the previous review session!


23/ Please pay attention to the coding style: indentation, space after special chars, empty line after method, etc. It's what I told you in the previous review session. Please do not repeat your issues! -- done


24/ I18n was not used (low priority, fix it when you have time). -- done
  - app/controllers/photos_controller.rb
  - app/controllers/albums_controller.rb


25/ Should not have always TRUE/FALSE condition in the app -- done

  - app/controllers/photos_controller.rb:6
  - app/controllers/albums_controller.rb:11

  => The condition `user != nil` never be FALSE because the user in this case never be NIL.


26/ Strange code

  - app/controllers/albums_controller.rb:12-13
  - app/controllers/albums_controller.rb:21-22

  => Why do you need both of `album_manage` and `@album_manage` here? Why is it just simple: `@album_manage = Album.order(created_at: :desc).where(private: false).page(params[:page]).per(6)`? --done

  - app/controllers/users_controller.rb#show

  => Should not order the records before counting. It's unnecessary.

  - app/controllers/users_controller.rb#check_album
  - app/controllers/users_controller.rb#check_photo

  => What is the purpose of the variable `i`? -- done


27/ Where are you defining these methods: `current_user.follow` and `current_user.unfollow`?


28/ Nearly do not take advantage of scope -- done

  - app/controllers/users_controller.rb#show

  => if you define a scope `new_first` (order(created_at: :desc)) in model `User`, you can avoid repeat `order(created_at: :desc)` everywhere, just be simple `.new_first`


### Bugs

1/ Link "Log in" in http://localhost:3000/users/password/new does not work -- Fixed 

2/ Style of the page http://localhost:3000/users/password/new is very different compared to other devise pages. -- Fixed 

3/ Missing validations in models. Need to follow the requirement. -- Fixed 

4/ What is `25%px` in this file `app/assets/stylesheets/albums.scss`? I wonder if it can work? -- Fixed 

5/ Wrong logics => -@album_manage: current_user.following to respone albums of all following users. I checked.

  - app/controllers/albums_controller.rb#feed_albums

  => With the current implementation, `@album_manage` is always albums of the last following user. `album_manage` should be albums of all following users

  - app/controllers/photos_controller.rb

  => Same as above