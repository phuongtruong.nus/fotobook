class AlbumsController < ApplicationController
	before_action :authenticate_user!
	before_action :set_album
	
	def feed_albums
		list_follow = current_user.following.ids
		@album_manage = Album.new_first.where(private: false, user_id: list_follow).page(params[:page]).per(6)
	end

	def discover_albums
		@album_manage = Album.new_first.where(private: false).page(params[:page]).per(6)
	end

	def new
	    @album = Album.new
	end

	def create
	    @album = current_user.albums.new(album_params)
		if @album.save && @album.images.attached? && @album.images.size <= 25
			flash[:notice] = "Album created successfully!"
			redirect_to current_user
		elsif 
			if @album.images.attached? == false
				flash[:alert] = "Please choose your images"
				render "new"
	    	elsif  @album.images.size >= 25
	    		flash[:alert] = "Maximum 25 images"
	    		render "new"
	  		else
	    		flash[:alert] = "#{@album.errors.full_messages.join(', ')}"
	    		render "new"
	    	end
		end
	end
	
	def show
    	
 	end

 	def edit
	end

	def update
		set_album()
		if params[:album][:private] != @album.private.to_s || params[:album][:title] != @album.title || params[:album][:description] != @album.description

			if params[:album][:images] != nil
				@album.update(title: params[:album][:title], private: params[:album][:private], images: params[:album][:images],description: params[:album][:description])
			else 
				@album.update(title: params[:album][:title], private: params[:album][:private], description: params[:album][:description])
			end 
			flash[:notice] = "Album edited successfully!"
			redirect_to current_user
		elsif params[:album][:images] == nil && @album.title == params[:album][:title]  && @album.description == params[:album][:description]  && @album.private.to_s == params[:album][:private] &&@album.images.size > 1
			@album.update(title: params[:album][:title], private: params[:album][:private], description: params[:album][:description])
			flash[:notice] = "Unchanged through time!"
			redirect_to current_user
		else 
			@album.update(title: params[:album][:title], private: params[:album][:private], description: params[:album][:description])
			flash[:alert] = "#{@album.errors.full_messages.join(', ')}"
			if @album.images.size < 1 
				flash[:alert] = "Album must have 1 photo"
			end
			redirect_to edit_album_path(@album)
		end
	end

	def destroy
		@album.destroy
		redirect_to current_user
	end

	def delete_images
	  	@album.images.purge
	end
	def delete_image_attachment
		# byebug
		@new_album = Album.find(params[:album_id])
	 	@image = @new_album.images.find_by(id: params[:image_id]).purge
	 	redirect_to edit_album_path(@new_album)
	end
 
  private
	def set_album
	    @album = Album.find_by(id: params[:id])
	end
    #strong parameters
    def album_params
      params.require(:album).permit(:title, :description, :private, images:[])
    end
end
