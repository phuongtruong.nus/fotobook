class LikesController < ApplicationController
	before_action :authenticate_user!

	def like
		current_user.likes.create(like_params)	
	end

	def unlike 
		current_user.likes.find_by(like_params).destroy	
	end

	private
    # Use callbacks to share common setup or constraints between actions.

    def like_params
    	params.permit( :likeable_type, :likeable_id)
    end
end
