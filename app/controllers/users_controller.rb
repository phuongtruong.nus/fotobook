class UsersController < ApplicationController
	before_action :authenticate_user!

 	def show
 		@user = User.find_by(id: params[:id])
 		#Use to respone album/photo per page
 		@albums = @user.albums.distinct.new_first.page(params[:page]).per(8)
 		@photos = @user.photos.distinct.new_first.page(params[:page]).per(8)
 		@followers = @user.followers.distinct.new_first.page(params[:page_follower]).per(12)
 		@following = @user.following.distinct.new_first.page(params[:page_following]).per(12)
 		#Check size 
 		@albums_size = @albums.total_count
 		@photos_size = @photos.total_count
 		@followers_size = @user.followers.distinct.new_first.size
 		@following_size = @user.following.distinct.new_first.size
 	end	
 	
end
