class Album < ApplicationRecord
	belongs_to :user
	has_many :likes, as: :likeable, dependent: :destroy
	has_many_attached :images

	#Create scope
	scope :new_first, -> { order(created_at: :desc)}
	
	#Validation
	validates :title, :description,  presence: true
	validates :title,    length: { in: 1..140 } 
	validates :description,    length: { in: 1..300 }
end