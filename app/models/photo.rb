class Photo < ApplicationRecord
	belongs_to :user
	has_many :likes, as: :likeable, dependent: :destroy
	has_one_attached :image

	#Create scope
	scope :new_first, -> { order(created_at: :desc)}

	#Validation
	validates :title, :description,  presence: true
	validates_associated :likes
	validates :title,    length: { in: 1..140 }
	validates :description,    length: { in: 1..300 }
end
