$(document).on('turbolinks:load', function(){
	$("#close-new-icon").hide();
	$("#upload-new-photo").show();
	$("#file-input-photo").click(function(){
		$("#upload-new-photo").hide();
		$("#close-new-icon").show();
		$("#new-image").show();
	});

	$("#close-new-icon").click(function(){
		$("#close-new-icon").hide();
		$("#upload-new-photo").show();
		$("#new-image").hide();
	});
});