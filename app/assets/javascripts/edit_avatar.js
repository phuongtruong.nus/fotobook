$(document).on('turbolinks:load', function(){
// <!-- Hide image present -->
	$("#close-icon").click(function(){
		$("#old-image").hide();
        $("#new-image").hide();
        $("#close-icon").hide();
        $("#upload-photo").show();
	});
 
	// <!--Begin Show preview image -->
  	$("#new-image").hide();
  	$("#upload-photo").hide();

  	$("#file-input-avatar").click(function(){
		  $("#old-image").hide();
      $("#upload-photo").hide();
      $("#close-icon").show();
      $("#new-image").show();
  	});
        
  	// End show preview image
});
