$(document).on('turbolinks:load', function() {
	$("#close-icon-old").click(function(){
		$(".album-old").hide;
	});
	function closeImage(stt) {	
	  	$("#old-image"+stt).hide();
	  	$("#gallery-album").hide();
	}
	$("#gallery-album").hide();
    // Multiple images preview in browser
    var imagesPreview = function(input, placeToInsertImagePreview) {
        if (input.files) {
            var filesAmount = input.files.length;
            for (i = 0; i < filesAmount; i++) {
                var reader = new FileReader();
                reader.onload = function(event) {
                	$(".photo-in").attr('src', event.target.result).appendTo(placeToInsertImagePreview);
                }
                reader.readAsDataURL(input.files[i]);
            }      
        }
    };
    $('#gallery-photo-add-album').on('change', function() {
        imagesPreview(this, 'div.photo-in');
        $("#gallery-album").show();
    });
});